/**
* @file Global configuration allowing for fine-tuning of config parameters and defaults.
* This will hopefully be the only file you'll ever have to edit.
* If you'd like help or something explained, don't hesitate to send me an email at tribex10@gmail.com.
* Or, if you're really desperate and need and answer right away, you can try caling 1-319-536-6016.
* @author Joshua Bemenderfer <tribex10@gmail.com>
*/

// Check for development mode.
var isDev = window.location.href.indexOf('localhost') !== -1

if (isDev) {
  console.warn('Running in Development Mode')
}

// Load config into a global variable. Not great, but easiest way to add comments to config.
window.MIDIDisplayConfig = {
  /** == FILE PATHS == **/
  // URL to the default background image.
  // Default: assets/images/background.png
  backgroundImage: 'assets/images/background.png',
  // Location to load soundfont from. This folder should contain files for each instrument, in the form of {instrument}-ogg.js & {instrument}-mp3.js
  // More soundfonts can be found at https://github.com/gleitz/midi-js-soundfonts
  // Default: assets/soundfont
  // NOTE: This cannot be changed while the page is loaded.
  soundFontLocation: 'assets/soundfont',
  // Directory of the sheet music .svg files for loaded MIDI files. Assumed to be the same location as the MIDI file itself if not set.
  // Default: Not Set
  // NOTE: The SVG files should have the same name as the MIDI files, but with a .svg extension.
  // Use the provided generate-svgs.bat (Windows) or generate-svgs.sh (Linux and hopefully macOS) to generate compatible SVGs from MIDI files.
  sheetMusicLocation: undefined,

  // Server that processes uploaded MIDI files into sheet music for access.
  // Default: http://localhost:9801 if running locally, or /api/v1 if running remotely. Avoid changing this unless you really know what you're doing.
  musicProcessingServer: isDev ? 'http://localhost:9801' : '/api/v1',

  /** == PROGRAM BEHAVIOR == **/
  // Is the note helper on or off by default
  // Default: Off
  noteHelper: true,
  // Whether or not to indicate note velocity through the opacity of the notes.
  // Default: Off
  useNoteOpacity: false,
  // Whether or not to show the song title at the beginning of the song.
  // Default: Off
  showSongTitle: false,
  // What percentage screen is blank to allow the notes to move downward before starting the music.
  // Default: 100 (%)
  playerStartDelay: 100,
  // Delay between cycles of the note player's auto-correct system, in milliseconds. A longer delay means smoother animation, but a shorter delay means higher accuracy.
  // Default: 900 (milliseconds)
  playerAutoCorrectDelay: 900,
  // Zoom targets to use for zooming. Pretty much don't modify this unless you want to spend tons of time on trial-and-error.
  zoomLevels: [
    // Level 1, Shows keys from C2 - B5
    {
      // What to display as the percentage for this zoom level. Doesn't need to be perfectly accurate.
      percentage: 200,
      // How far from the left of the piano roll (in percents) do you want the viewport to be? (Use this to align to keys)
      offsetLeft: 17.4, // 17.4 is C2
      // How large should the zoom be? x is horizontal, y is vertical. A value of 1 is unzoomed.
      scale: {
        x: 1.87, // 1.87 is actually 187%, not 200%. Shh, don't tell anyone.
        y: 1.6 // Adjust Y as needed so the keys don't seem too stretched or squished.
      },
      // The larger the view distance is, the shorter the piano roll notes will be, but the slower the piano roll will move. Aim for about 30 to 70
      viewDistance: 50
    },
    // Level 2, Shows keys from C2 - B6
    {
      percentage: 150,
      offsetLeft: 17.4,
      scale: {
        x: 1.49, // 1.49 is actually 149%. Shh, don't tell anyone.
        y: 1.3
      },
      viewDistance: 40
    },
    // Level 3, Whole keyboard
    {
      percentage: 100,
      offsetLeft: 0,
      scale: {
        x: 1,
        y: 1
      },
      viewDistance: 30
    }
  ],
  // Default zoom level selected from above. Zero-indexed (0 selects the first zoom level)
  zoomLevel: 1,

  // Tuna.js Audio effects. See MIDI.js & TunaJS repositories. https://github.com/mudcube/MIDI.js, https://github.com/Theodeus/tuna
  audioEffects: [
    {
      type: 'Convolver',
      highCut: 22050,
      lowCut: 20,
      dryLevel: 1,
      wetLevel: 0,
      level: 1,
      impulse: 'assets/impulses/marble_hall.wav',
      bypass: 0
    }
  ],

  // Default amount of reverb. Between 0 and 1
  reverbAmount: 0,

  // If audio effects are on by default or not. Disabling this will disable reverb adjustment
  audioEffectsOn: true,

  // Function to generate colors based on track ids. Passed the track number and expected to return a hex-formatted color, though RGBA will work as well.
  // Default: [Function]
  trackColorGenerator: function (trackId) {
    var colors = [
      '#d52b2b',
      '#18b0d0',
      '#d34c59',
      '#7bcb09',
      '#dfaf0f',
      '#d52b2b',
      '#10d5c9',
      '#c83fa9'
    ]

    return colors[trackId] || window.randomColor() || '#da0'
  },
  // Default background opacity
  // Default: 0.7
  backgroundOpacity: 0.7,

  // Function to be run when the application first loads. Passed the State object (src/state/state.js) for you to do with as you please.
  // Further events can be bound to with State.eventBus.$on(EventName<String>, Handler<Function>)
  onload: function (State) {
    return
  },

  /** == ENABLED FEATURES == **/
  // Fine-tuned enabling and disabling of certain features of the program.
  // Set a section to false to disable it entirely.
  enabledFeatures: {
    // Hides everything but the main controls on a small screen to keep the program useable.
    responsiveMode: true,

    // Panel that allows you to upload MIDI and Background files.
    uploader: {
      midi: true,
      background: true
    },

    // Panel that allows you to control layers.
    layersPanel: {
      mute: true,
      hide: true
    },

    // Main song controls.
    mainControls: {
      rewind: true,
      playPause: true,
      stop: true,
      volume: true,
      progress: {
        // If disabled, you can't skip around the song from the progress indicator.
        skip: true
      }
    },

    // Sub controls / Settings.
    subControls: {
      // Disabling mode will hide sheet music.
      mode: true,
      reverb: true,
      speed: true,
      noteHelper: true,
      zoom: true,
      fullscreen: true
    }
  }
}
