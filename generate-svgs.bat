REM MuseScore must be installed globally for this script to work!
@echo off

echo Beginning SVG generation...
REM For each file that ends with *.mid...
for %%F in (*.mid) do (
  REM Use MuseScore to convert the file to a SVG. Try both 32-bit and 64-bit locations. Supress errors.
  "C:\Program Files\MuseScore 2\bin\MuseScore.exe" %%F -o "%%~nF.svg" >nul 2>&1
  "C:\Program Files (x86)\MuseScore 2\bin\MuseScore.exe" %%F -o "%%~nF.svg" >nul 2>&1
  echo Generated "%%~nF.svg" from "%%F"
)
