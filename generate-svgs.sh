#!/bin/bash
# MuseScore must be installed globally for this script to work!

echo Beginning SVG generation...
# For each file that ends with *.mid...
for file in *.mid; do
    # Use MuseScore to convert the file to a SVG.
    musescore "$file" -o "`basename "$file" .mid`.svg" >/dev/null 2>&1
    echo Generated "`basename "$file" .mid`.svg" from "$file"
done
