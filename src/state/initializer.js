/**
* @file Handles parsing of the query string and loading songs and such onload.
* @author Joshua Bemenderfer <tribex10@gmail.com>
*/

export const Initializer = function (State) {
  if (window.location.search) {
    const params = new window.URLSearchParams(window.location.search.slice(1))

    State.loadFiles({
      mid: params.get('mid'),
      svg: params.get('svg')
    }, () => {
      State.togglePlayState()
    })
  }
}
