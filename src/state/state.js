/**
* @file Global state object for managing changes to the application between components.
* @author Joshua Bemenderfer <tribex10@gmail.com>
*/

import Vue from 'vue'
import {Initializer} from './initializer'
import {ConfigLoader} from '../util/config-loader'
import {TrackConfigManager} from '../util/track-config-manager'
import {WebMIDIHook} from '../util/webmidi-hook'
import {KeyboardHook} from '../util/keyboard-hook'

const MIDI = window.MIDI

export const EventBus = new Vue()

export const State = {
  // Config
  config: window.MIDIDisplayConfig,

  // Internal
  keyPositions: [],
  displayMode: 0,
  subControlsOpen: false,
  layerControlsOpen: false,
  SearchOpen: false,
  tracks: {},
  missingInitialNotes: [],
  isFullscreen: false,
  canLoadSheetMusic: false,

  // MIDI State
  WebMIDIHook,
  KeyboardHook,
  playState: 0, // 0 = Stopped, 1 = Paused, 2 = Playing, 3 = Pre-Start
  currentPosition: 0,
  totalLength: 0,
  activeNotes: {},
  volume: 127,
  MIDI: window.MIDI,
  // Event Listeners
  eventBus: EventBus,
  ConfigLoader,

  // File Data
  currentFiles: {
    midi: null,
    title: null,
    sheet: null
  },

  setup (callback) {
    // Add trailing slash just in-case.
    if (State.config.soundFontLocation.split('').slice(-1)[0] !== '/') {
      State.config.soundFontLocation += '/'
    }

    MIDI.loadPlugin({
      soundfontUrl: State.config.soundFontLocation,
      onsuccess: callback
    })
  },

  // Methods
  loadFiles (files, callback, timeWarp) {
    if (!files.mid) return
    if (!files.title) files.title = files.mid.split('.mid').join('')

    if (files.mid.indexOf('.mid') !== -1) {
      if (!files.sheet && !State.config.sheetMusicLocation) {
        files.sheet = files.mid.split('.mid')
      } else if (!files.sheet) {
        const basename = files.mid.split('/').slice(-1)[0].split('.mid')

        // Add trailing slash just in-case.
        if (State.config.sheetMusicLocation.split('').slice(-1)[0] !== '/') {
          State.config.sheetMusicLocation += '/'
        }

        files.sheet = State.config.sheetMusicLocation + basename
      }
    }

    State.tracks = {}
    State.missingInitialNotes = []
    State.currentFiles = files

    MIDI.Player.BPM = null
    MIDI.Player.timeWarp = timeWarp || 1
    MIDI.Player.loadFile(files.mid, () => {
      buildDatasets()

      MIDI.toggleEffects(State.config.audioEffectsOn)
      callback ? callback() : null

      EventBus.$emit('player.loadFiles')
    })
  },

  reload (callback, timeWarp) {
    State.loadFiles(State.currentFiles, callback, timeWarp)
  },

  // Switch between start, play, and pause depending on the song's state.
  togglePlayState (callback) {
    if (State.playState === 0 && MIDI.Player.currentTime === 0) {
      State.start(callback)
    } else if (State.playState === 1) {
      State.play(callback)
    } else if (State.playState === 2) {
      State.pause()
      callback ? callback() : null
      // Ignore any commands if the play state is pre-start.
    } else if (State.playState === 3) {
      return
    }
  },

  play (callback) {
    State.playState = 2
    MIDI.Player.start(() => {
      callback ? callback() : null

      EventBus.$emit('player.play')
    }, true)
  },

  start (callback) {
    if (MIDI.Player.currentTime === 0) {
      State.playState = 3
      EventBus.$emit('player.pre-start')

      setTimeout(() => {
        // Just in-case another button was pressed, reset to the "stopped" state if the player wasn't in the "play" state.
        if (State.playState === 3) {
          State.play(callback)
        } else if (State.playState !== 2) {
          State.playState = 0
        }
      }, State.config.playerStartDelay * State.getViewDistance())
    } else {
      State.play(callback)
    }
  },

  pause () {
    State.playState = 1
    MIDI.Player.pause()
    EventBus.$emit('player.pause')
  },

  stop () {
    State.playState = 0
    MIDI.Player.pause()
    State.currentPosition = 0
    MIDI.Player.currentTime = 0
    EventBus.$emit('player.stop')
  },

  setVolume (volume) {
    State.volume = +volume
    MIDI.WebAudio.setVolume(0, +volume)
  },

  adjustReverb (amount) {
    amount = Math.min(Math.max(amount, 0), 1)
    State.config.reverbAmount = amount
    MIDI.adjustReverb(amount)
  },

  zoomIn () {
    if (State.config.zoomLevel > 0) {
      State.config.zoomLevel -= 1
    }

    if (State.playState > 1) {
      State.pause()

      EventBus.$emit('player.zoomChange')
      setTimeout(() => {
        State.play()
      }, 100)
    }
  },

  zoomOut () {
    if (State.config.zoomLevel < State.config.zoomLevels.length - 1) {
      State.config.zoomLevel += 1
    }

    if (State.playState > 1) {
      State.pause()

      EventBus.$emit('player.zoomChange')
      setTimeout(() => {
        State.play()
      }, 100)
    }
  },

  toggleAudioEffects () {
    State.config.audioEffectsOn = !State.config.audioEffectsOn

    MIDI.toggleEffects(State.config.audioEffectsOn)
  },

  getViewDistance () {
    return State.config.zoomLevels[State.config.zoomLevel].viewDistance
  },

  skipTo (position) {
    if (State.playState > 1) {
      State.pause()
      MIDI.Player.currentTime = position
      EventBus.$emit('player.skipTo')

      setTimeout(() => {
        State.play()
      }, 100)
    }
  },

  setTimeWarp (timeWarp) {
    const currentTime = State.MIDI.Player.currentTime

    // Stop the current track, if any.
    State.stop()

    // Adjust the time warp.
    State.reload(() => {
      // Set the state to playing.
      State.playState = 2
      // Skip to the old current time.
      State.skipTo(currentTime * timeWarp)
    }, timeWarp)
  },

  noteOn (track, channel, note, velocity, triggerMIDI) {
    if (!State.tracks[track] && track !== -1) return

    if (triggerMIDI) MIDI.noteOn(track, channel, note, velocity)

    if (track === -1 || (!State.tracks[track].mute && State.tracks[track].show)) {
      State.activeNotes[note] = true
      State.keyPositions[note - 21].isActive = true
      State.keyPositions[note - 21].activeColor = track === -1 ? null : State.tracks[track].color
      State.keyPositions[note - 21].opacity = State.config.useNoteOpacity ? Math.max(velocity / 127, 0.6) : 1
    }
  },

  noteOff (channel, note, triggerMIDI) {
    if (triggerMIDI) MIDI.noteOff(channel, note)

    delete State.activeNotes[note]
    State.keyPositions[note - 21].isActive = false
    State.keyPositions[note - 21].activeColor = null
    State.keyPositions[note - 21].opacity = 1
  }
}

// Initialize loading.
State.setup(() => {
  Initializer(State)
  MIDI.setEffects(State.config.audioEffects, State.config.audioEffectsOn)
  MIDI.adjustReverb(State.config.reverbAmount)

  State.config.onload ? State.config.onload() : null
})

// Hacky way to work-around missing notes at time 0. buildDatasets caches the missing notes, then we play them manually when the song starts.
EventBus.$on('player.play', () => {
  if (State.currentPosition === 0) {
    State.missingInitialNotes.forEach(note => {
      if (!State.tracks[note.track].mute) {
        State.noteOn(note.track, note.channel, note.noteNumber, note.velocity, true)

        setTimeout(() => {
          State.noteOff(note.channel, note.noteNumber, true)
        }, note.end)
      }
    })
  }
})

// Builds all the needed data for state.tracks from the MIDI data.
// Be warned, this is a mess. (Not really, but it looks like one.)
const buildDatasets = () => {
  const tracks = {}
  const activeNotes = {}
  let position = 0

  // Iterate through every event
  MIDI.Player.data.forEach(item => {
    // Moves the position in time forward by this item's specified time value.
    position += item[1]
    // Metadata.
    const noteMeta = item[0]
    // Shortcut to noteMeta.event
    const noteEvent = noteMeta.event

    // Get track names, if they exist.
    if (noteEvent.type === 'meta') {
      if (noteEvent.subtype === 'trackName') {
        const track = noteMeta.track

        // Add this track to the tracks array if it doesn't exist, since the order of track meta events vs note events is not guarenteed.
        if (!tracks[track]) {
          const trackConfig = TrackConfigManager.getConfig(State.currentFiles.title, track)

          tracks[track] = {
            color: trackConfig.color || State.config.trackColorGenerator(track),
            show: trackConfig.show != null ? trackConfig.show : true,
            mute: trackConfig.mute || false,
            notes: [],
            name: noteEvent.text,
            saveConfig: function () {
              TrackConfigManager.saveConfig(State.currentFiles.title, track, {
                color: this.color,
                show: this.show,
                mute: this.mute
              })
            }
          }

          if (trackConfig.mute) State.MIDI.WebAudio.setTrackVolume(track, 0)
        } else {
          tracks[track].name = noteEvent.text
        }
      }
      // The channel event is usually noteOn/noteOff
    } else if (noteEvent.type === 'channel') {
      // When the note turns on, add an event to each track's note list. The order of noteOn/noteOff events should be consistent.
      if (noteEvent.subtype === 'noteOn') {
        const track = noteMeta.track
        if (!tracks[track]) {
          const trackConfig = TrackConfigManager.getConfig(State.currentFiles.title, track)

          tracks[track] = {
            color: trackConfig.color || State.config.trackColorGenerator(track),
            show: trackConfig.show != null ? trackConfig.show : true,
            mute: trackConfig.mute || false,
            notes: [],
            name: '',
            saveConfig: function () {
              TrackConfigManager.saveConfig(State.currentFiles.title, track, {
                color: this.color,
                show: this.show,
                mute: this.mute
              })
            }
          }

          if (trackConfig.mute) State.MIDI.WebAudio.setTrackVolume(track, 0)
        }

        if (!activeNotes[track]) activeNotes[track] = {}

        const note = {
          key: MIDI.noteToKey[noteEvent.noteNumber],
          track: track,
          start: position,
          end: position + 1,
          channel: noteEvent.channel,
          noteNumber: noteEvent.noteNumber,
          velocity: noteEvent.velocity
        }

        tracks[track].notes.push(note)

        if (position === 0) {
          State.missingInitialNotes.push(note)
        }

        // Mark this note as active, so that a noteOff event can be used to set the end position later.
        activeNotes[track][noteEvent.noteNumber] = tracks[track].notes.length - 1
        // When a noteOff is given, turn off the previous note in this track that was marked as active and set its end position.
      } else if (noteEvent.subtype === 'noteOff') {
        const track = noteMeta.track
        const activeNote = tracks[track].notes[activeNotes[track][noteEvent.noteNumber]]

        if (activeNote) {
          activeNote.end = position
        }

        delete activeNotes[track][noteEvent.noteNumber]
      }
    }
  })

  State.tracks = tracks
}

// Listen for active notes.
MIDI.Player.addListener(data => {
  State.currentPosition = MIDI.Player.currentTime
  // Note turns on.
  if (data.message === 144) {
    State.noteOn(data.track, 0, data.note, data.velocity, false)

    // Note turns off.
  } else if (data.message === 128) {
    State.noteOff(0, data.note, false)
  }
})

// Build Key Position Array
const blackKeys = [1, 3, 6, 8, 10]

let keyPosInOctave = 9
let offsetLeft = 0
for (let i = 21; i < 109; i++) {
  const isBlackKey = blackKeys.indexOf(keyPosInOctave) !== -1

  State.keyPositions[i - 21] = {
    note: i,
    isBlack: isBlackKey,
    isActive: false,
    activeColor: null,
    offsetLeft: isBlackKey ? offsetLeft - 0.7 : offsetLeft
  }

  offsetLeft += isBlackKey ? 0 : 1.92

  if (keyPosInOctave === 11) {
    keyPosInOctave = 0
  } else {
    keyPosInOctave++
  }
}

// Fullscreen shim
const body = document.body
body.requestFullscreen = body.requestFullscreen || body.mozRequestFullScreen || body.webkitRequestFullscreen || body.msRequestFullscreen
document.exitFullscreen = document.exitFullscreen || document.mozCancelFullScreen || document.webkitExitFullscreen || document.msExitFullscreen
