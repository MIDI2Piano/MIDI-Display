import {State} from '../state/state'

const $ = window.$

export const ConfigLoader = {
  handleConfigMessage (event) {
    if (event.data.channel !== 'MIDIPlayer.setConfig') return

    if (typeof event.data === 'object') {
      try {
        $.extend(true, State.config, event.data)
        event.source.postMessage({
          channel: 'MIDIPlayer.setConfig',
          status: 'success'
        }, '*')
      } catch (e) {
        try {
          event.source.postMessage({
            channel: 'MIDIPlayer.setConfig',
            status: 'failure',
            error: e
          }, '*')
        } catch (e) {}
      }
    } else {
      event.source.postMessage({
        channel: 'MIDIPlayer.setConfig',
        status: 'failure',
        error: new Error('TypeError: Configuration requires passing an object.')
      }, '*')
    }
  },

  handleConfigHash () {
    if (window.location.hash.length) {
      const hash = window.location.hash.substring(1)

      try {
        const ConfigObject = JSON.parse(decodeURIComponent(hash))
        $.extend(true, State.config, ConfigObject)

        window.parent.postMessage({
          channel: 'MIDIPlayer.setConfig',
          status: 'success'
        }, '*')
      } catch (e) {
        try {
          window.parent.postMessage({
            channel: 'MIDIPlayer.setConfig',
            status: 'failure',
            error: e
          }, '*')
        } catch (e) {}
      }
    }
  }
}

window.addEventListener('message', ConfigLoader.handleConfigMessage, false)
window.addEventListener('hashchange', ConfigLoader.handleConfigHash, false)
ConfigLoader.handleConfigHash()
