const reqwest = window.reqwest

import {State} from '../state/state'

export const FileUploader = {
  uploadMIDIFile (filename, file, callback) {
    const formData = new window.FormData()
    formData.append('midi', file)

    reqwest({
      url: `${State.config.musicProcessingServer}/midi`,
      method: 'post',
      processData: false,
      data: formData
    })
    .then(() => {
      callback ? callback(`${filename.split('.mid').join('')}`) : null
    })
    .catch(e => {
      callback ? callback(null) : null
    })
  }
}
