/**
* @file Handles Keyboard input
* @uathor Joshua Bemenderfer
**/
import {State} from '../state/state'

const keyMappings = ['a', 'w', 's', 'e', 'd', 'f', 't', 'g', 'y', 'h', 'u', 'j', 'k', 'o', 'l', 'p', ';', '\'']

let octave = 3
let velocity = 127

export const KeyboardHook = {
  changeOctave (increment) {
    octave = Math.max(Math.min(octave + increment, 7), 0)
  },

  changeVelocity (increment) {
    velocity = Math.max(Math.min(velocity + increment, 127), 1)
  },

  handleKeyDown ($event) {
    $event.preventRepeat()

    const keyIndex = keyMappings.indexOf($event.key.toLowerCase())

    if (keyIndex !== -1) {
      State.noteOn(-1, 0, (keyIndex + (12 * octave)) + 12, velocity, true)
    }
  },

  handleKeyUp ($event) {
    const keyIndex = keyMappings.indexOf($event.key.toLowerCase())

    if (keyIndex !== -1) {
      State.noteOff(0, (keyIndex + (12 * octave)) + 12, true)
    }
  }
}

// Key bindings
window.keyboardJS.bind(keyMappings, KeyboardHook.handleKeyDown, KeyboardHook.handleKeyUp)

// Octave decrease binding
window.keyboardJS.bind('z', () => {
  KeyboardHook.changeOctave(-1)
})

// Octave increase binding
window.keyboardJS.bind('x', () => {
  KeyboardHook.changeOctave(1)
})

// Velocity decrease binding
window.keyboardJS.bind('c', () => {
  KeyboardHook.changeVelocity(-1)
})

// Velocity increase binding
window.keyboardJS.bind('v', () => {
  KeyboardHook.changeVelocity(1)
})
