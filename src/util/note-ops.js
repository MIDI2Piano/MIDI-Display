import Vue from 'vue'

const keys = 'ABCDEFG'

export const NoteOps = {
  midiKeyToRealKey (midiKey) {
    if (midiKey.length === 3) {
      const key = midiKey.split('')[0]
      const keyIndex = keys.indexOf(key)
      const newIndex = keyIndex === 0 ? keys.length - 1 : keyIndex - 1

      return `${keys[newIndex]}#`
    } else {
      return midiKey.split('')[0] === 'C' ? midiKey : midiKey.split('')[0]
    }
  }
}

Vue.filter('midiKeyToRealKey', key => NoteOps.midiKeyToRealKey(key))
