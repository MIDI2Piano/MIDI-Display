import Vue from 'vue'

export const msToTime = function (milliseconds) {
  const minutes = Math.floor((milliseconds / 1000 / 60) << 0)
  const seconds = Math.floor((milliseconds / 1000) % 60)

  return minutes + ':' + (seconds < 10 ? '0' + seconds : seconds)
}

Vue.filter('msToTime', ms => msToTime(ms))
