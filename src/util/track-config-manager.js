
export const TrackConfigManager = {
  saveConfig (songName, track, config) {
    window.localStorage.setItem(`${songName}-${track}-config`, JSON.stringify(config))
  },

  getConfig (songName, track) {
    try {
      return JSON.parse(window.localStorage.getItem(`${songName}-${track}-config`)) || {}
    } catch (e) {
      return {}
    }
  }
}
