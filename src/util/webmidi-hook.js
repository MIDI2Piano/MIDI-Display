/**
 * @file Handles WebMIDI input
 * @uathor Joshua Bemenderfer
 **/

import {State} from '../state/state'

export const WebMIDIHook = {
  hasWebMIDI: false,
  midiAccess: null,

  requestAccess (callback) {
    if (navigator.requestMIDIAccess) {
      WebMIDIHook.hasWebMIDI = true

      navigator.requestMIDIAccess({
        sysex: false
      })
      .then(callback, (e) => {
        console.log(e)
      })
    }
  },

  accessGained (data) {
    WebMIDIHook.midiAccess = data

    var inputs = data.inputs.values()

    // Loop over all available inputs and listen for any MIDI input.
    for (let input = inputs.next(); input && !input.done; input = inputs.next()) {
      // Each time there is a midi message call the onMIDIMessage function.
      input.value.onmidimessage = WebMIDIHook.onMessage
    }
  },

  onMessage (message) {
    const data = message.data
    // const cmd = data[0] >> 4
    const channel = data[0] & 0xf
    const type = data[0] & 0xf0
    const note = data[1]
    const velocity = data[2]

    switch (type) {
      case 144: // noteOn message
        State.MIDI.noteOn(-1, channel, note, velocity)
        State.activeNotes[note] = true
        State.keyPositions[note - 21].isActive = true

        break
      case 128: // noteOff message
        State.MIDI.noteOff(channel, note)
        delete State.activeNotes[note]
        State.keyPositions[note - 21].isActive = false
        break
    }
  }
}

WebMIDIHook.requestAccess(WebMIDIHook.accessGained)
